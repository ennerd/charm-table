<?php
declare(strict_types=1);

namespace Charm\Table;

use Countable;
use IteratorAggregate;
use JsonSerializable;

/**
 * @method self restrict(mixed ...$params)
 */
interface TableInterface extends IteratorAggregate, Countable, JsonSerializable
{
    /**
     * Fetch the first matching row and return it.
     */
    public function one(): mixed;

    public function where(string $column, string $op, $value): self;

    public function order(string $column, bool $desc = false): self;

    public function limit(int $offset): self;

    public function offset(int $count): self;

    public function lt(string $column, $value): self;

    public function lte(string $column, $value): self;

    public function gt(string $column, $value): self;

    public function gte(string $column, $value): self;

    public function eq(string $column, $value): self;
}
