<?php
declare(strict_types=1);

namespace Charm\Table;

/**
 * Similar to an array, but with added functionality relevant for
 * filtering, transforming and sub-querying.
 */
class IteratorTable extends AbstractTable
{
    protected $iteratorAggregate;

    public function __construct(callable $iteratorAggregate)
    {
        $this->iteratorAggregate;
    }

    public function getIterator(): iterable
    {
        $generator = $this->iteratorAggregate;
        yield from $generator();
    }
}
