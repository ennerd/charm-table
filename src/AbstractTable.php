<?php
declare(strict_types=1);

namespace Charm\Table;

use Charm\App\ApiResponse;
use Charm\Dispatcher\Http\ResponseTrait;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Similar to an array, but with added functionality relevant for
 * filtering, transforming and sub-querying.
 */
abstract class AbstractTable implements TableInterface, ResponseInterface
{
    use ResponseTrait;

    protected $offset = 0;
    protected $limit = 1000;

    public function getMessageBody() {
        return json_encode($this);
    }

    public function parseServerRequest(ServerRequestInterface $request): self {
        $table = clone $this;
        $get = $request->getQueryParams();
        foreach ($get as $key => $valueOrOperator) {
            if (\is_array($valueOrOperator)) {
                foreach ($valueOrOperator as $operator => $value) {
                    if (\in_array($operator, ['eq', 'lt', 'lte', 'gt', 'gte'])) {
                        $table = $table->$operator($key, $value);
                    }
                }
            } else {
                $table = $table->eq($key, $valueOrOperator);
            }
        }
        return $table;
    }

    public function niu_getResponse(ServerRequestInterface $request): ResponseInterface
    {
        $table = clone $this;
        $get = $request->getQueryParams();
        foreach ($get as $key => $valueOrOperator) {
            if (\is_array($valueOrOperator)) {
                foreach ($valueOrOperator as $operator => $value) {
                    if (\in_array($operator, ['eq', 'lt', 'lte', 'gt', 'gte'])) {
                        $table = $table->$operator($key, $value);
                    }
                }
            }
            $table = $table->eq($key, $valueOrOperator);
        }

        return new ApiResponse($request, $table);
    }

    public function one(): mixed
    {
        foreach ($this->limit(1) as $result) {
            return $result;
        }

        return null;
    }

    public function compare(object $row, string $column, $value): float
    {
        if (\is_int($value) || \is_float($value)) {
            return $row->$column - $value;
        }

        return \Charm\I18n\Collator::instance()->compare($row->$column, $value);
    }

    /**
     * Wraps each of the 'lt', 'lte', 'gt', 'gte' and 'eq' operators.
     */
    final public function where(string $column, string $op, $value): self
    {
        switch ($op) {
            case '<': return $this->lt($column, $value);
            case '<=': return $this->lte($column, $value);
            case '>': return $this->gt($column, $value);
            case '>=': return $this->gte($column, $value);
            case '=': return $this->eq($column, $value);
            default:
                throw new Error("Expects an operator of: '<', '<=', '>', '>=', '='");
        }
    }

    public function order(string $column, bool $desc = false): self
    {
        return new IteratorTable(function () use ($column, $desc) {
            $rows = iterator_to_array($this);
            usort($rows, function ($a, $b) use ($column, $desc) {
                return $this->compare($a, $column, $b->$column) * ($desc ? -1 : 1);
            });
            yield from $rows;
        });
    }

    public function limit(int $limit): self
    {
        $result = new IteratorTable(function () use ($limit) {
            foreach ($this as $row) {
                if (0 === $limit--) {
                    return;
                }
                yield $row;
            }
        });
        $result->limit = $limit;

        return $result;
    }

    public function offset(int $offset): self
    {
        $result = new IteratorTable(function () use ($offset) {
            foreach ($this as $row) {
                if ($offset-- > 0) {
                    continue;
                }
                yield $row;
            }
        });
        $result->offset = $offset;

        return $result;
    }

    public function lt(string $column, $value): self
    {
        return new IteratorTable(function () use ($column, $value) {
            foreach ($this as $row) {
                if ($this->compare($row, $column, $value) < 0) {
                    yield $row;
                }
            }
        });
    }

    public function lte(string $column, $value): self
    {
        return new IteratorTable(function () use ($column, $value) {
            foreach ($this as $row) {
                if ($this->compare($row, $column, $value) <= 0) {
                    yield $row;
                }
            }
        });
    }

    public function gt(string $column, $value): self
    {
        return new IteratorTable(function () use ($column, $value) {
            foreach ($this as $row) {
                if ($this->compare($row, $column, $value) > 0) {
                    yield $row;
                }
            }
        });
    }

    public function gte(string $column, $value): self
    {
        return new IteratorTable(function () use ($column, $value) {
            foreach ($this as $row) {
                if ($this->compare($row, $column, $value) >= 0) {
                    yield $row;
                }
            }
        });
    }

    public function eq(string $column, $value): self
    {
        return new IteratorTable(function () use ($column, $value) {
            foreach ($this as $row) {
                if (0 === $this->compare($row, $column, $value)) {
                    yield $row;
                }
            }
        });
    }

    public function jsonSerialize()
    {
        $rows = [];
        $rowCount = 0;
        foreach ($this as $row) {
            ++$rowCount;
            $rows[] = $row;
        }

        if ($rowCount === $this->limit) {
            // We may have more rows available, so we count
            $totalRows = $this->count();
        } else {
            $totalRows = $rowCount;
        }

        return [
            'rows' => $rows,
            'rowCount' => $rowCount,
            'offset' => $this->offset,
            'limit' => $this->limit,
            'totalRows' => $totalRows,
        ];
    }
}
