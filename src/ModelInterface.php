<?php
declare(strict_types=1);

namespace Charm\Table;

use JsonSerializable;
use Psr\Http\Server\RequestHandlerInterface;

interface ModelInterface extends JsonSerializable
{
    public static function all(): TableInterface;
}
